#!/usr/bin/make -f

BUILD_TREE:=build-tree

WRAPPERS=
TOOLS=hardening-check
MANPAGES=

$(BUILD_TREE)/stamp-build: $(WRAPPERS) $(TOOLS) $(MANPAGES) Makefile
	mkdir -p $(BUILD_TREE)

	# Construct tools.
	install $(TOOLS) $(BUILD_TREE)/
	# Do not use "shell" here because it eats newlines. We want those.
	perl -pi -e "s/^my %libc;/my %libc = (\n$$(perl hardening-check --find-libc-functions /bin/ls)\n);/;" $(BUILD_TREE)/hardening-check

	# Construct man pages.
	pod2man hardening-check > $(BUILD_TREE)/hardening-check.1

	touch $(BUILD_TREE)/stamp-build

check: $(BUILD_TREE)/stamp-build
	make -C tests check

clean:
	rm -rf $(BUILD_TREE)

install:
	rm -rf $(BUILD_TREE)

.PHONY: check clean
